var template = `

<div class="container-fluid" id="search_result">
    <div class="row" style="margin-top: 30px;">
        <div class="col-xs-12 col-md-2"></div>
            <div class="col-xs-12 col-md-8">
                <div class="container-fluid" id="search_result_card">
                    <div class="row">
                        <div id="search_result_name" style="padding: 10px; background-color: #2000af; border-top-left-radius: 5px; border-top-right-radius: 5px;">
                            <h3 style="color: white">{{description}}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <a href="{{url}}" target="_blank">
                        <div style="height: 400px; overflow-y: scroll;"> <img src="{{url}}" width="100%"/> </div>
                        </a>
                        <a id="loadButton" href="#" style="display: none;">
                            <div class="col-xs-12" style="text-align: center;">
                                <h3 style="margin-top: 10px; margin-bottom: 10px;">
                                    Документ слишком тяжелый. <br>Загрузить предпросмотр
                                </h3>
                            </div>
                        </a>
                    </div>

                    <div class="row" id="search_result_footer">
                        <div class="rating_button">
                            <button postId="{{id}}" id="upvoteButton" type="button" class="btn btn-success btn-md" aria-label="Left Align">
                                <span class="glyphicon glyphicon-triangle-top" aria-hidden="true"></span>
                            </button>
                        </div>

                        <div class="rating_chip">
                            {{rating}}
                        </div>

                        <div class="rating_button">
                            <button postId="{{id}}" id="downvoteButton" type="button" class="btn btn-danger btn-md" aria-label="Left Align">
                                <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
                            </button>
                        </div>

                        <a href="#" id="courseLink" class="serach_result_tag">
                            <div class="tag_chip">
                                <img src="subject.png" alt="Person" width="50" height="50">
                                <span>{{course}}</span>
                            </div>
                        </a>
                        <a href="#" id="yearLink" class="serach_result_tag">
                            <div class="tag_chip">
                                <img src="year.png" alt="Year" width="50" height="50">
                                <span>{{year}}</span>
                            </div>
                        </a>
                        <a href="#" id="tutorLink" class="serach_result_tag">
                            <div class="tag_chip">
                                <img src="teacher.png" alt="Tutor" width="50" height="50">
                                <span>{{tutor}}</span>
                            </div> 
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`

GL = 0

var tag = {"C": "course", "T": "tutor", "Y": "year"}
var str = ["description", "url", "rating", "course", "year", "tutor", "id"]
var getListItem = function(obj) {
    obj["rating"] += '';
    obj["url"] = "http://" + ipport + "/api/get_img/?id=" + obj["id"];
    strItem = template.replace("{{url}}", obj["url"]  + "#page=" + obj["page"][0]);
    strItem = strItem.replace("{{id}}", obj["id"]);
    obj["url"] += "&page=" + obj["page"][0];
    obj["course"] = obj["tutor"] = obj["year"] = ""
    if (obj["heavy"])
        obj["url"] = "X" + obj["url"];
    for (var i = 0; i < obj.tags.length; ++i) {
        console.log(tag[obj.tags[i][0]]);
        obj[tag[obj.tags[i][0]]] = obj.tags[i].substring(tag[obj.tags[i][0]].length + 2);
    }
    for (var i = 0; i < str.length; ++i) 
        strItem = strItem.replace("{{" + str[i] + "}}", obj[str[i]]);

    res = $(strItem);
    if (obj["heavy"]) {
        GL = res;
        $("#loadButton", res).show().siblings().hide();
    }
    return res;
}