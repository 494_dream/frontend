ipport = "138.68.97.0:8000"

activeYear = "*";

buttonBlocks = 2;
tutors = [];
courses = [];

tryUnlockButtons = function() {
    console.log("WOW");
    buttonBlocks -= 1;
    if (buttonBlocks <= 0) {
        $("#searchDropdownButton").removeClass("disabled");
        $("#uploadButton").removeClass("disabled");
    }
}

// GET  api/get_rating/?id=FILE_ID -> {rating: NUMBER}
// POST api/change_rating/?id=FILE_ID&delta=
handleVote = function(vote, button) {
    if (button.hasClass("disabled"))
        return;
    button.addClass("disabled");
    $("button", button.parent().siblings(".rating_button")).addClass("disabled");

    $.ajax({
        type: "POST",
        url: "http://" + ipport + "/api/change_rating/",
        data: {id: button.attr("postId"), delta: vote},
        success: function(response) {
            BTN = button;
            button.parent().siblings(".rating_chip").hide().text(response["rating"]).fadeIn();
            console.log(response);
        }
    });
}

doSearch = function(query, tagsStr) {
    console.log("SEARCH:", query, tagsStr)
    $.ajax({
        url: "http://" + ipport + "/api/search/?q=" + query + "&tags=" + tagsStr,
        success: function(data) {
            $("#queryInput").val("");
            $("#termPickerSearch > a:first").click();
            $("#courseSearchInput").val("");
            $("#tutorSearchInput").val("");
            console.log("search done");
            console.log(data);
            $("#queryResult").empty();
            if (data.length == 0) {
                $("#queryResult").append($(
                    `
                    <div class="empty-search-text">
                        <h1>Ничего не найдено</h1>
                        <p>вы можете быть первым, кто загрузит конспект на эту тему</p>
                    </div>
                    `));
                reuturn;
            }
            for (var i = 0; i < data.length; ++i) {
                $("#queryResult").append(getListItem(data[i]));
            }
        }
    });
}

handleSearch = function() {
    var query = $("#queryInput").val();
    var courseStr = $("#courseSearchInput").val();
    var tutorStr = $("#tutorSearchInput").val();
    var yearStr = $("#termPickerSearch > a.active").prevAll().length + "";
    var tagsStr = "";
    tagsStr += (courseStr !== "" ? "Course: " + courseStr + "," : "");
    tagsStr += (tutorStr !== "" ? "Tutor: " + tutorStr + "," : "");
    tagsStr += (yearStr !== "0" ? "Year: " + yearStr + "," : "");
    if (tagsStr.length > 0)
        tagsStr = tagsStr.slice(0, -1);
    doSearch(query, tagsStr)
}

handleResize = function() {
    var wdth = Math.min($(window).width(), 500);
    $(".dropdown.dropdown-lg .dropdown-menu").css("min-width", wdth + "px");
}

buildTypeahead = function() {
    $.ajax({
        url: "http://" + ipport + "/api/get_tutors/",
        success: function(data) {
            console.log("tutors loaded");
            console.log(data);
            tutors = data;
            tutors2 = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: tutors
            });
            $(".tutorInput").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: "tutors",
                source: tutors2
            });
            console.log("OK tutors autocompletion");
            tryUnlockButtons();
        }
    });

    // loading courses
    $.ajax({
        url: "http://" + ipport + "/api/get_courses/",
        success: function(data) {
            console.log("courses loaded");
            console.log(data);
            courses = data;
            courses2 = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: courses
            });
            $(".courseInput").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: "courses",
                source: courses2
            });
            console.log("OK courses autocompletion");
            tryUnlockButtons();
        }
    });
}

$(document).ready(function() {
    buildTypeahead();

    $("#sendButton").click(send_data);

    $("#helpButton").click(function() {
        startIntro();
    });

    $(document).on("click", "#courseLink", function() {doSearch("", "Course: " + $("div > span", this).text())});
    $(document).on("click", "#yearLink", function() {doSearch("", "Year: " + $("div > span", this).text())});
    $(document).on("click", "#tutorLink", function() {doSearch("", "Tutor: " + $("div > span", this).text())});

    $(document).on("click", "#loadButton", function() {
        $(this).hide().siblings().show();
        var img = $("div > img", $(this).siblings());
        console.log(img);
        img.attr("src", img.attr("src").slice(1));
    })

    $("#uploadModal").on("hidden.bs.modal", function() {
        $("#URL").val("");
        $("#termPickerUpload > a:first").click();
        $("#courseAddInput").val("");
        $("#tutorAddInput").val("");
        $("#discr").val("");
    });

    handleResize();
    $(window).resize(handleResize);

    activeYear = 0;

    $(document).on("click", "#upvoteButton", function(){handleVote(1, $(this))});
    $(document).on("click", "#downvoteButton", function(){handleVote(-1, $(this))});

    $(".searchOnEnter").keypress(function(event) {
        if (event.keyCode == 13)
            $("#searchButton").click();
    });

    $("#searchButton").click(handleSearch);

    $("#termPickerUpload > .btn").click(function() {
        activeYear = $(this).text()[0];
        $(this).addClass("active").siblings().removeClass("active");
    });
    $("#termPickerSearch > .btn").click(function() {
        $(this).addClass("active").siblings().removeClass("active");
    });
});